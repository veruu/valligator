"""Simple one-function validators."""
import logging
import re


def contains_keyval(cover, patches, key):
    """Check if a 'key: <value>' is present."""
    pattern = re.compile(r'^{}:\s+\S+$'.format(key), re.I | re.M)

    if pattern.search(cover):
        return True

    to_return = True
    for index, patch in enumerate(patches):
        if not pattern.search(patch):
            logging.error('Patch %d doesn\'t contain %s!', index + 1, key)
            to_return = False

    return to_return


def subject_contains_keyword(cover, patches, key):
    """Check the subject contains a required keyword"""
    to_return = True

    pattern = re.compile(r'^Subject:\s.*\[.*{}.*\]'.format(key),
                         re.M)

    if cover and not pattern.search(cover):
        logging.error('Cover letter doesn\'t contain %s!', key)
        to_return = False

    for index, patch in enumerate(patches):
        if not pattern.search(patch):
            to_return = False
            logging.error('Patch %d doesn\'t contain %s!', index + 1, key)

    return to_return


def must_have_cover(cover, patches, must):
    """
    Check the existence of a cover letter when required.
    """

    to_return = True
    must = must.lower()

    if must not in ["true", "false", "series"]:
        logging.error("Unrecognized argument: %s", must)
        return False

    if not cover:
        if must == "true":
            to_return = False
            logging.error("You need to provide with a cover letter.")
        if must == "series" and len(patches) > 1:
            to_return = False
            logging.error("%d patches provided. You need to attach the cover.",
                          len(patches))
    return to_return
