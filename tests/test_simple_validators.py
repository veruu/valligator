"""Tests for valligator.simple_validators."""
import re
import unittest

from valligator import simple_validators


class TestContainsKeyval(unittest.TestCase):
    """Tests for simple_validators.contains_keyval()."""
    def test_no_keyval(self):
        """Verify contains_keyval fails if the key is not present."""
        patch = 'This is a sample text in the patch'
        self.assertFalse(
            simple_validators.contains_keyval('', [patch], 'key')
        )

    def test_keyval_missing_patch(self):
        """
        Verify contains_keyval fails if the key is not present in all patches.
        """
        patch_true = 'text\nkey: value'
        patch_false = 'This is a sample text in the patch'
        self.assertFalse(simple_validators.contains_keyval(
            '', [patch_true, patch_false], 'key'
        ))

    def test_keyval_in_cover(self):
        """Verify contains_keyval passes if the key is in the cover."""
        cover = 'text\nkey: value'
        patch = 'This is a sample text in the patch'
        self.assertTrue(
            simple_validators.contains_keyval(cover, [patch], 'key')
        )

    def test_keyval_in_patches(self):
        """
        Verify contains_keyval passes if the key is present in all patches.
        """
        patches = ['text\nkey: value', 'something\n\nkey: value2\n\n']
        self.assertTrue(
            simple_validators.contains_keyval('', patches, 'key')
        )


class TestSubjectContainsKeyword(unittest.TestCase):
    """Tests for simple_validators.contains_keyval()."""

    def test_no_cover_no_keyword_in_patch(self):
        """
        Verify subject_contains_keyword fails if given only patches,
        keyword is not in the patch subject
        """
        patch = 'This is a sample text in the patch'
        self.assertFalse(
            simple_validators.subject_contains_keyword('', [patch], 'key')
        )

    def test_no_cover_no_keyword_in_one_patch(self):
        """
        Verify subject_contains_keyword fails if the keyword
        is not present in only one of the patches.
        """
        patch_true = 'text\nSubject: [ key ]: value'
        patch_false = 'This is a sample text in the patch'
        self.assertFalse(simple_validators.subject_contains_keyword(
            '', [patch_true, patch_false], 'key'
        ))

    def test_keyword_in_cover_and_patches(self):
        """
        Verify subject_contains_keyword passes if the keyword
        is in the cover and in the patches
        """
        cover = 'Subject: [ key: value]'
        patch = 'Subject: [This is a sample text in the patch with the key ]'
        self.assertTrue(
            simple_validators.subject_contains_keyword(cover, [patch], 'key')
        )

    def test_no_cover_keyword_in_all_patches_subject(self):
        """
        Verify subject_contains_keyword passes if the keyword
        is present in all patches given no cover.
        """
        patches = ['Subject: [ text key ]',
                   'Subject: [be key 1/21 qsdf/as]']
        self.assertTrue(
            simple_validators.subject_contains_keyword('', patches, 'key')
        )

    def test_no_keyword_one_patch(self):
        """
        Verify subject_contains_keyword fails if the keyword
        is not present in only one of the patches given a cover
        and multiple patches.
        """
        cover = 'Subject: [ key: value]'
        patch_true = 'text\nSubject: [ key ]: value'
        patch_false = 'This is a sample text in the patch'
        self.assertFalse(simple_validators.subject_contains_keyword(
            cover, [patch_true, patch_false], 'key'
        ))


class TestMustHaveCover(unittest.TestCase):
    """Tests for existence of Cover Letter when it is required."""

    def test_required_and_no_cover(self):
        """
        Verify must_have_cover fails when a cover is required
        and it is not included.
        """

        cover = ''
        must = 'trUE'
        patches = ['patch1 content', 'patch2 content']
        self.assertFalse(simple_validators.must_have_cover(
            cover, patches, must
        ))

    def test_required_and_cover(self):
        """
        Verify must_have_cover passes when a cover is required
        and it is included.
        """

        cover = 'cover content'
        must = 'TrUe'
        patches = ['patch1 content', 'patch2 content']
        self.assertTrue(simple_validators.must_have_cover(
            cover, patches, must
        ))

    def test_not_required_and_no_cover(self):
        """
        Verify must_have_cover passes when a cover is not required
        and it is not included.
        """

        cover = ''
        must = 'false'
        patches = ['patch1 content', 'patch2 content']
        self.assertTrue(simple_validators.must_have_cover(
            cover, patches, must
        ))

    def test_not_required_and_cover(self):
        """
        Verify must_have_cover passes when a cover is not required
        and it is included.
        """

        cover = 'cover content'
        must = 'False'
        patches = ['patch1 content', 'patch2 content']
        self.assertTrue(simple_validators.must_have_cover(
            cover, patches, must
        ))

    def test_bad_word(self):
        """
        Verify must_have_cover fails and alerts when an
        unrecognized argument is provided (ignores the check)
        and returns immediately.
        """

        cover = 'cover content'
        must = 'qwerty'
        patches = ['patch1 content', 'patch2 content']
        self.assertFalse(simple_validators.must_have_cover(
            cover, patches, must
        ))

    def test_series_more_patches_no_cover(self):
        """
        Verify must_have_cover fails when series is activated,
        more than one patch is provided and there is no cover.
        """

        cover = ''
        must = 'series'
        patches = ['patch1 content', 'patch2 content']
        self.assertFalse(simple_validators.must_have_cover(
            cover, patches, must
        ))

    def test_series_more_patches_cover(self):
        """
        Verify must_have_cover passes when series is activated,
        more than one patch is provided and there is a cover.
        """

        cover = 'cover content'
        must = 'series'
        patches = ['patch1 content', 'patch2 content']
        self.assertTrue(simple_validators.must_have_cover(
            cover, patches, must
        ))

    def test_series_less_patches_no_cover(self):
        """
        Verify must_have_cover passes when series is activated
        and one or less patches are provided even though there
        is no cover.
        """

        cover = ''
        must = 'series'
        patches = ['patch2 content']
        self.assertTrue(simple_validators.must_have_cover(
            cover, patches, must
        ))
